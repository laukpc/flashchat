//
//  Message.swift
//  Flash Chat iOS13
//
//  Created by Laurynas Kapacinskas on 2020-10-12.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import Foundation

struct Message {
    let sender : String
    let body : String
}
